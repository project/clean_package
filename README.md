# Drupal Clean Package Composer Plugin

Removes the unnecessary files for the specific package based on the given rules.

**Be careful:** This plugin removes packages files. Test defined rules
properly. Remove only those files which are totally unnecessary for the project
needs.

## Install

Require this package in your `composer.json`:
```
composer require drupal/clean_package
```

## Examples

See example rules in [examples](./examples) directory.
More examples are welcome to suggest.
Also, the `composer.clean.json` file will be performed during
the installation of `drupal/clean_package` module. So this README.md file and other
unnecessary files and directories of the plugin will be removed.

## Usage

This plugin will work automatically on any installed packages. You can find
this plugin is helpful in case some dependencies of the project are
access in the public directory of the project and contains the files that contain
information that the attackers may use to determine possible
vulnerabilities. By defining the rules in the root package, you can manage
which files of the package need to be removed during installation.

The package needs to define one of the extra options:
- `clean-file`
- `clean-rules`

`clean-file` is the string value relative to the package path to the file
containing the rules. The `clean-file` will be removed, and the other files
based on the rules except for the root package.

Example of the `composer.json`:
```json
    "name": "my/package",
    ...
    "extra": {
        "clean-file": "composer.clean.json"
    }
```
Example of the `composer.clean.json`:
```json
{
    "clean-rules": {
        "some-other/package": [
            "file1.txt",
            ...
        ]
    }
}
```

`clean-rules` is the JSON object. It contains keys as the name of the package
and the value as an array of strings representing the pattern or the file path
of the package. See
[glob function documentation](https://www.php.net/manual/ru/function.glob.php)
regarding pattern definition. If the `clean-file` is already defined in `extra`
section of `composer.json` file, then rules defined in `clean-rules` would be
ignored.

Example of the `composer.json`:
```json
    "name": "my/package",
    ...
    "extra": {
        "clean-rules": {
            "some-other/package": [
                "file1.txt",
                ...
            ]
        }
    }
```
