<?php

declare(strict_types=1);

namespace Drupal\Clean;

use Composer\Composer;
use Composer\Config;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\InstalledVersions;
use Composer\Installer\PackageEvent;
use Composer\Installer\PackageEvents;
use Composer\IO\IOInterface;
use Composer\Json\JsonFile;
use Composer\Package\PackageInterface;
use Composer\Plugin\PluginInterface;
use Composer\Util\Filesystem;
use Exception;
use Seld\JsonLint\ParsingException;

use function explode;
use function glob;
use function realpath;
use function trim;

class Plugin implements PluginInterface, EventSubscriberInterface
{
    /** @var  Composer */
    protected $composer;
    /** @var  IOInterface */
    protected $io;
    /** @var  Config */
    protected $config;
    /** @var  Filesystem */
    protected $filesystem;

    /**
     * {@inheritDoc}
     */
    public function activate(Composer $composer, IOInterface $io)
    {
        $this->composer   = $composer;
        $this->io         = $io;
        $this->config     = $composer->getConfig();
        $this->filesystem = new Filesystem();
    }

    /**
     * {@inheritDoc}
     */
    public function deactivate(Composer $composer, IOInterface $io)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function uninstall(Composer $composer, IOInterface $io)
    {
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents() : array
    {
        return [
            PackageEvents::POST_PACKAGE_INSTALL => [
                ['onPostPackageInstall', 0],
            ],
            PackageEvents::POST_PACKAGE_UPDATE  => [
                ['onPostPackageUpdate', 0],
            ],
        ];
    }

    /**
     * Function to run after a package has been installed
     */
    public function onPostPackageInstall(PackageEvent $event)
    {
        $this->cleanPackage(
            $event->getOperation()->getPackage()
        );
    }

    /**
     * Function to run after a package has been updated
     */
    public function onPostPackageUpdate(PackageEvent $event)
    {
        $this->cleanPackage(
            $event->getOperation()->getTargetPackage()
        );
    }

    /**
     * Clean a package, based on its rules.
     *
     * @param PackageInterface $package The package to clean
     * @throws ParsingException
     */
    protected function cleanPackage(PackageInterface $package) : void
    {
        $packageName = $package->getPrettyName();
        $packageDir  = InstalledVersions::getInstallPath($package->getPrettyName());

        // Perform root package defined rules if exists.
        $this->runRules(
            $this->getCleanRules($this->composer->getPackage())[$package->getPrettyName()] ?? [],
            $packageDir
        );
        // Perform current package defined rules if exists.
        foreach ($this->getCleanRules($package) as $packageNameToClean => $rules) {
            $directory = $packageNameToClean === $packageName
                ? $packageDir
                : InstalledVersions::getInstallPath($packageNameToClean);
            if (! empty($directory)) {
                $this->runRules(
                    $rules[$package->getPrettyName()] ?? [],
                    $directory
                );
            }
        }
        $cleanFile = $this->getCleanFilePath($package);
        if ($cleanFile) {
            // Perform clean file to clean rules.
            $allRules[] = [$cleanFile];
            $this->runRules(
                [$cleanFile],
                $packageDir
            );
        }
    }

    /**
     * Perform operations based on the given rules.
     *
     * @param array  $rules            Array of rules.
     * @param string $packageDirectory Package directory.
     */
    protected function runRules(array $rules, string $packageDirectory) : void
    {
        foreach ((array) $rules as $part) {
            // Split patterns for single globs (should be max 260 chars).
            $patterns = explode(' ', trim($part));
            foreach ($patterns as $pattern) {
                try {
                    foreach (glob("${packageDirectory}/${pattern}") as $file) {
                        $this->filesystem->remove($file);
                    }
                } catch (Exception $e) {
                    $this->io->write("Could not parse ${packageDirectory} (${pattern}): " . $e->getMessage());
                }
            }
        }
    }

    /**
     * File path getter.
     *
     * @param PackageInterface $package Package instance.
     * @return string|null File path to clean rules.
     */
    protected function getCleanFilePath(PackageInterface $package) : ?string
    {
        return $package->getExtra()['clean-file'] ?? null;
    }

    /**
     * Getter of rules from the defined file.
     *
     * @param PackageInterface $package Package instance.
     * @return array Array of rules.
     * @throws ParsingException
     */
    protected function getCleanFileRules(PackageInterface $package) : array
    {
        $rootRulesFile = $this->getCleanFilePath($package);
        if (! empty($rootRulesFile)) {
            $packageDirectory = InstalledVersions::getInstallPath($package->getPrettyName());
            $json             = new JsonFile(
                $this->filesystem->normalizePath(
                    realpath($packageDirectory . '/' . $rootRulesFile)
                )
            );
            return $this->getRulesFromExtra($json->read());
        }
        return [];
    }

    /**
     * Read rules form provided extra array.
     *
     * @param array $extra Array represent extra section of composer.json.
     * @return array Array of rule patterns.
     */
    protected function getRulesFromExtra(array $extra = []) : array
    {
        return $extra['clean-rules'] ?? [];
    }

    /**
     * Rules base getter.
     *
     * @param PackageInterface $package Package instance
     * @return array Array of rule patterns.
     * @throws ParsingException
     */
    protected function getCleanRules(PackageInterface $package) : array
    {
        return $this->getCleanFileRules($package) ?: $this->getRulesFromExtra($package->getExtra());
    }
}
